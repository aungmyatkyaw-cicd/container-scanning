FROM registry.gitlab.com/security-products/container-scanning:7

# Switch to root user temporarily to install packages
USER root

RUN apt-get update && apt-get install -y xml-twig-tools jq && rm -rf /var/lib/apt/lists/*

# Switch back to the non-root user
USER gitlab
